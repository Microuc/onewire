/**
  ******************************************************************************
  * @file    SED1520.h
  * @author  Matthias Becker
  * @brief   This file provides code for using SED1520 based displays
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SED1520_H
#define __SED1520_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
typedef struct {
	uint8_t	Overdrive;
	void	(*GPIO_Write)(uint8_t);
	uint8_t	(*GPIO_Read)(void);
	void	(*delay)(uint32_t);
	struct	{float A, B, C, D, E, F, G, H, I, J;}Timing;

	uint8_t value;
	uint8_t (*Add)(uint8_t, uint8_t);
}tOW;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/
/**	Function to initiate the OneWire-Interface
 * 	All Interface-Parameters have to be set before using this function. Otherwise it will through an error.
 *
 * 	@param pOneWire OneWire interface typedef
 *
 * 	@return initial state
 * 	@return 9 OVerdrive-param out of range
 */
uint8_t OW_Init(tOW pOneWire);

/** Function to reset the interface
 *
 */
uint8_t OW_Reset(tOW p_OneWire);

/**
 *
 */
uint8_t OW_Read(tOW p_OneWire);

/**
 *
 */
void OW_Write(tOW p_OneWire, uint8_t par_val);

/* Private types -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif /* __SED1520_H */

/****END OF FILE****/
