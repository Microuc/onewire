/**
  ******************************************************************************
  * @file    onewire.c
  * @author  Matthias Becker
  * @brief   This file provides code for using onewire-interface
  ******************************************************************************
  */
  
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "onewire.h"

/* Private typedef -----------------------------------------------------------*/



/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
//uint8_t A, B, C, D, E, F, G, H, I, J;
/* Private function prototypes -----------------------------------------------*/

/* Exported functions --------------------------------------------------------*/
uint8_t OW_Init(tOW pOneWire){
	pOneWire.value = 1;

	if((pOneWire.Overdrive != 0) & (pOneWire.Overdrive != 1)) return 9;
	if(pOneWire.GPIO_Read == NULL) return 10;
	if(pOneWire.GPIO_Write == NULL) return 11;
	if(pOneWire.delay == NULL) return 12;

	if(pOneWire.Overdrive == 0){
		pOneWire.Timing.A = 6;
		pOneWire.Timing.B = 64;
		pOneWire.Timing.C = 60;
		pOneWire.Timing.D = 10;
		pOneWire.Timing.E = 9;
		pOneWire.Timing.F = 55;
		pOneWire.Timing.G = 0;
		pOneWire.Timing.H = 480;
		pOneWire.Timing.I = 70;
		pOneWire.Timing.J = 410;
	}else if(pOneWire.Overdrive == 1){
		pOneWire.Timing.A = 1.0;
		pOneWire.Timing.B = 7.5;
		pOneWire.Timing.C = 7.5;
		pOneWire.Timing.D = 2.5;
		pOneWire.Timing.E = 1.0;
		pOneWire.Timing.F = 7.0;
		pOneWire.Timing.G = 2.5;
		pOneWire.Timing.H = 70.0;
		pOneWire.Timing.I = 8.5;
		pOneWire.Timing.J = 40.0;
	}

	return 1;
}


uint8_t OW_Reset(tOW p_OneWire){
	p_OneWire.delay(p_OneWire.Timing.G);
	p_OneWire.GPIO_Write(0);
	p_OneWire.delay(p_OneWire.Timing.H);
	p_OneWire.GPIO_Write(1);
	p_OneWire.delay(p_OneWire.Timing.I);
	uint8_t presence = p_OneWire.GPIO_Read();
	p_OneWire.delay(p_OneWire.Timing.J);

	return presence;
}

void OW_Write(tOW p_OneWire, uint8_t par_val){

	switch(par_val){
		case 0:
			p_OneWire.GPIO_Write(0);
			p_OneWire.delay(p_OneWire.Timing.C);
			p_OneWire.GPIO_Write(1);
			p_OneWire.delay(p_OneWire.Timing.D);
			break;
		case 1:
			p_OneWire.GPIO_Write(0);
			p_OneWire.delay(p_OneWire.Timing.A);
			p_OneWire.GPIO_Write(1);
			p_OneWire.delay(p_OneWire.Timing.B);
			break;
	}
}

uint8_t OW_Read(tOW p_OneWire){
	p_OneWire.GPIO_Write(0);
	p_OneWire.delay(p_OneWire.Timing.A);
	p_OneWire.GPIO_Write(1);
	p_OneWire.delay(p_OneWire.Timing.E);
	uint8_t result = p_OneWire.GPIO_Read();
	p_OneWire.delay(p_OneWire.Timing.F);

	return result;
}
/* Private functions ---------------------------------------------------------*/

/****END OF FILE****/
